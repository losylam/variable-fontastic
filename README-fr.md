# Créer une font variable avec Processing, fontforge et fontmake

## Principe

fontmake permet de créer une fonte variable à partir de plusieurs fichier UFO et un fichier .designspace qui définit les différents axes.

Pour générer de simples polices de base, j'utilise la bibliothèque Fontastic de Processing et en particulier l'exemple RandomFont, qui génére un quadrilatère aléatoire par glyph. Il suffit ensuite de créer le fichier .designspace adéquat.

## Fontastic

[Fontastic](http://code.andreaskoller.com/libraries/fontastic/) est une bibliothèque processing pour générer des fichiers de fonte au format ttf (par défaut) et woff (pas testé).

J'utilse l'example RandomFont pour générer trois fontes, en appuyant simplement sur la barre d'espace 3 fois après avoir lancé le sketch.

Les fontes obtenues sont dans le répertoire "data" de l'exemple au format ttf

## Conversion en ufo avec fontforge

J'utilise fontforge sur ubuntu 18.04 depuis les dépots fournis sur le [site](http://fontforge.github.io/en-US/downloads/gnulinux-dl/).

``` shell
sudo add-apt-repository ppa:fontforge/fontforge;
sudo apt-get update;
sudo apt-get install fontforge;
```

Il suffit d'ouvrir un à un les fichiers .ttf et de générer la fonte au format ufo.

## fontmake

[Fontmake](https://github.com/googlefonts/fontmake) est un logiciel en python permettant fournit par google sous une licence Apache. Il permet de compiler des fichiers ufo (ou glyphs) pour créer des fichier de font opentype ou true type.

J'utilise pip et virtualenv pour l'installer dans un environnement virtuel python

``` shell
virtualenv -p python3 venv
source ./venv/bin/activate
pip install fontmake
```

J'aurais pu aussi l'installer de façon globale sans environnement virtuel
``` shell
sudo pip install fontmake
```

## fichier .designspace

Le fichier .designspace permet de définir les axes de la fonte variable et le lien vers les fichier ufos. Il s'agit d'un fichier xml qu'on peut aisément modifier à la main.

On foit d'abord définir les axes, c'est à dire leur nom, tag, les valeurs par défaut, minimale et maximale.

``` xml
<axes>
  <axis maximum="900" minimum="100" tag="wght" default="900" name="weight">
    <labelname xml:lang="en">Weight</labelname>
  </axis>
</axes>
```

On doit ensuite définir les sources, c'est à dire les fichiers ufo qui servent à définir les extrémités de chacun des axes. Chaque source est liée à un fichier ufo.

On définit ensuite le placement de chacune de ces sources sur les axes.

``` xml
<sources>
  <source familyname="RandomFont" name="RandomFont" stylename="RandomFont" filename="RandomFont0001.ufo">
    <location>
      <dimension xvalue="100" name="weight"/>
    </location>
  </source>
  <source familyname="RandomFont" name="RandomFont" stylename="RandomFont-Mod" filename="RandomFont0002.ufo">
    <location>
      <dimension xvalue="900" name="weight"/>
    </location>
  </source>
</sources>
```

Dans le cas de plusieurs axes, il faut que les dimensions de chaque axe soit définie pour chaque source.

## Création de la fonte avec fontmake

Pour créer la fonte variable, on execute fontmake en précisant le fichier .designspace `-m FILENAME.designspace` et que le format de fichier est un format de font variable `-o variable` 

``` shell
fontmake -m RandomFont.designspace -o variable
```

Le fichier ttf obtenu est dans le dossier variable_ttf.
