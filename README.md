# How to create a variable font with Processing, Fontforge and Fontmake

![variable-fontastic](variable-fontastic.gif)

[Fontmake](https://github.com/googlefonts/fontmake) allow to create a variable font from several ufo files by writing a .designspace file that defines different axes.

## Fontastic

To generate simple base font, we use the example RandomFont shipped with the [Fontastic](http://code.andreaskoller.com/libraries/fontastic/) Processing library. This sketch generate a random quad for each glyph and save them as a ttf file.

To generate font, just hit the spacebar from within the running sketch. 

## Ttf to Ufo with fontforge

I use fontforge on ubuntu 18.04 with the provided PPA on the fontforge [website](http://fontforge.github.io/en-US/downloads/gnulinux-dl/).

``` shell
sudo add-apt-repository ppa:fontforge/fontforge;
sudo apt-get update;
sudo apt-get install fontforge;
```

Then we just open one by one the .ttf file and use `file > generate fonts` to export them as ufo.

## fontmake

I use pip and virtualenv to install it:

``` shell
virtualenv -p python3 venv
source ./venv/bin/activate
pip install fontmake
```

But I could have done without:
``` shell
sudo pip install fontmake
```

## The .designspace file

The .designspace is a simple xml that allow to define one or several axes for the variable font and the location of ufo files.

We first have to define the axes name, tag and different values:
``` xml
<axes>
  <axis maximum="900" minimum="100" tag="wght" default="900" name="weight">
    <labelname xml:lang="en">Weight</labelname>
  </axis>
</axes>
```

We then define sources that are in fact links to the ufo files and location on the various axes.

``` xml
<sources>
  <source familyname="RandomFont" name="RandomFont" stylename="RandomFont" filename="RandomFont0001.ufo">
    <location>
      <dimension xvalue="100" name="weight"/>
    </location>
  </source>
  <source familyname="RandomFont" name="RandomFont" stylename="RandomFont-Mod" filename="RandomFont0002.ufo">
    <location>
      <dimension xvalue="900" name="weight"/>
    </location>
  </source>
</sources>
```

(In the case of multiple axes, we need to define the location on each axes for each source.)

## Using fontmake to create the variable font

Just type this in the terminal:
``` shell
fontmake -m RandomFont.designspace -o variable
```

And the truetype variable font appears in a variable_ttf directory. 
